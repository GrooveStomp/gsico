#include <stdio.h>
#include <stdlib.h>
#include <alloca.h>
#include <inttypes.h>

#ifndef int_least24_t
#define gs_int24 int_least32_t
#else
#define gs_int24 int_least24_t
#endif

#include "gs.h"

typedef struct
{
        float Hue;
        float Saturation;
        float Value;
} hsv;

typedef struct
{
        int Red;
        int Green;
        int Blue;
} rgb;

gs_int24
PackTripletIntoInt(void *Param)
{
        rgb Triplet = *(rgb *)Param;
        gs_int24 Result;
        Result = (Triplet.Red << 16) & (Triplet.Green << 8) & (Triplet.Blue);

        return(Result);
}

rgb
StringToRgb(char *Rgb)
{
        unsigned int RgbStringLength = GSStringLength(Rgb);
        for(int I = 0; I < RgbStringLength; I++)
                Rgb[I] = GSCharUpcase(Rgb[I]);

        uint32_t Temp;
        sscanf(Rgb, "%X", &Temp);

        rgb Result;
        Result.Red = Temp >> 16;
        Result.Green = (Temp & 0xFF00) >> 8;
        Result.Blue = Temp & 0xFF;

        return(Result);
}

gs_bool
RgbToString(rgb Rgb, char *String, unsigned int StringLength)
{
        if(StringLength < 7) return(false);

        snprintf(String, 7, "%X%X%X", Rgb.Red, Rgb.Blue, Rgb.Green);
        return(true);
}

rgb
HsvToRgb(hsv Hsv)
{

        float C = Hsv.Value * Hsv.Saturation;
        float X = (1 - abs((int)(Hsv.Hue / 60) % 2 - 1));
        float M = Hsv.Value - C;

        float Rp, Gp, Bp;
        if(0 < Hsv.Hue && Hsv.Hue < 60)
        {
                Rp = C; Gp = X; Bp = 0;
        }
        else if(60 < Hsv.Hue && Hsv.Hue < 120)
        {
                Rp = X; Gp = C; Bp = 0;
        }
        else if(120 < Hsv.Hue && Hsv.Hue < 180)
        {
                Rp = 0; Gp = C; Bp = X;
        }
        else if(180 < Hsv.Hue && Hsv.Hue < 240)
        {
                Rp = 0; Gp = X; Bp = C;
        }
        else if(240 < Hsv.Hue && Hsv.Hue < 300)
        {
                Rp = X; Gp = 0; Bp = C;
        }
        else if(300 < Hsv.Hue && Hsv.Hue < 360)
        {
                Rp = C; Gp = 0; Bp = X;
        }

        rgb Result;
        Result.Red = (Rp + M) * 255;
        Result.Green = (Gp + M) * 255;
        Result.Blue = (Bp + M) * 255;

        return(Result);
}

hsv
RgbToHsv(rgb Rgb)
{
        int Hue = 0;
        int Saturation = 0;
        int Value = 0;

        float Red = Rgb.Red / 255.0;
        float Green = Rgb.Green / 255.0;
        float Blue = Rgb.Blue / 255.0;

        float MinRgb = GSMin((float)Red, GSMin((float)Green, (float)Blue));
        float MaxRgb = GSMax((float)Red, GSMax((float)Green, (float)Blue));

        /* Black/Gray/White */
        if(MinRgb == MaxRgb)
        {
                hsv Result;
                Result.Hue = 0;
                Result.Saturation = 0;
                Result.Value = MinRgb;
                return(Result);
        }

        /* All other colors */
        float D;
        if(Red == MinRgb)
                D = Green - Blue;
        else if(Blue == MinRgb)
                D = Red - Green;
        else
                D = Blue - Red;

        float H;
        if(Red == MinRgb)
                H = 3;
        else if(Blue == MinRgb)
                H = 1;
        else
                H = 5;

        Hue = 60 * (H - D / (MaxRgb - MinRgb));
        Saturation = (MaxRgb - MinRgb) / MaxRgb;
        Value = MaxRgb;

        hsv Result = { Hue, Saturation, Value };

        return(Result);
}

void
Usage(char *ProgramName)
{
        printf("Usage: %s hex-color-code\n", ProgramName);
        puts("");
        puts("hex-color-code: eg.: FF04A6");
        puts("");
        puts("Options:");
        puts("\t-h|--help:      Display this help text");
        exit(EXIT_SUCCESS);
}

int
main(int ArgCount, char **ArgStrings)
{
        gs_args *Args = (gs_args*)alloca(sizeof(gs_args));
        GSArgsInit(Args, ArgCount, ArgStrings);
        if(GSArgsHelpWanted(Args) || ArgCount == 1) Usage(GSArgsProgramName(Args));

        rgb Rgb = StringToRgb(GSArgsAtIndex(Args, 1));
        hsv Hsv = RgbToHsv(Rgb);

        printf("String: %s\n", GSArgsAtIndex(Args, 1));
        printf("StringToRgb: %X,%X,%X\n", Rgb.Red, Rgb.Green, Rgb.Blue);
        printf("RgbToHsv: %f,%f,%f\n", Hsv.Hue, Hsv.Saturation, Hsv.Value);

        char RgbString[8] = { 0 };
        gs_bool Succeeded = RgbToString(Rgb, RgbString, 8);
        if(!Succeeded) GSAbortWithMessage("Failed writing RGB triplet to string\n");
        printf("RgbToString: %s\n", RgbString);

        rgb Converted = HsvToRgb(Hsv);
        printf("HsvToRgb: %X,%X,%X\n", Converted.Red, Converted.Green, Converted.Blue);
}
